use crate::known::KnownTest;
use rayon::prelude::*;

pub struct Scaler {
    means: Vec<f64>,
    std_devs: Vec<f64>,
}

impl Scaler {
    pub fn new(dataset: &[KnownTest]) -> Self {
        let inputs_len = dataset[0].inputs.len();
        let mut scaler = Scaler {
            means: vec![0.0; inputs_len],
            std_devs: vec![0.0; inputs_len],
        };
        scaler.fit(dataset);
        scaler
    }

    pub fn transform_test(&self, test: &mut KnownTest) {
        self.transform(&mut test.inputs);
    }

    pub fn transform(&self, data: &mut [f64]) {
        data.par_iter_mut().enumerate().for_each(|(i, x)| {
            *x -= self.means[i];
            *x /= self.std_devs[i];
        });
    }

    fn fit(&mut self, dataset: &[KnownTest]) {
        for test in dataset {
            for (col, val) in test.inputs.iter().enumerate() {
                self.means[col] += val;
                self.std_devs[col] += val.powi(2);
            }
        }
        self.means.par_iter_mut().for_each(|m| {
            *m /= dataset.len() as f64;
        });
        self.std_devs.par_iter_mut().for_each(|s| {
            *s = if *s != 0.0 { s.sqrt() } else { 1.0 };
        });
    }
}
