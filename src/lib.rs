pub mod activation;
mod ann;
mod known;
mod neuron;
pub mod scaler;

pub use ann::EpochResult;
pub use ann::State;
pub use ann::ANN;
pub use known::KnownTest;
