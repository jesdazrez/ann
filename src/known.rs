use serde_derive::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Serialize, Deserialize)]
pub struct KnownTest {
    pub inputs: Vec<f64>,
    pub outputs: Vec<f64>,
}

impl KnownTest {
    pub fn new(inputs: Vec<f64>, outputs: Vec<f64>) -> Self {
        Self { inputs, outputs }
    }

    pub fn class(&self) -> Option<usize> {
        self.outputs
            .iter()
            .enumerate()
            .max_by(|x, y| x.1.partial_cmp(y.1).unwrap_or(Ordering::Equal))
            .map(|(i, _)| i)
    }
}
