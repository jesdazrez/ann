# ANN - Artificial Neural Network in Rust

A Rust implementation of an artificial neural network.

Still a WIP and created to learn and demonstrate a parallel approach to the
minibatch version of the backpropagation algorithm.
